# Command Line / Help

Use `-h` to display the command line help.

```bash
# if you installed `trucotrack` as a global app
trucotrack -h

# if you are working with the source code:
python -m src -h
```
```

usage: trucotrack [-h] [--video VIDEO] [--camera CAMERA] [--interval INTERVAL]
                  [--first FIRST] [--last LAST] [--flip FLIP]
                  [--mp_hands_max MP_HANDS_MAX]
                  [--mp_hands_static MP_HANDS_STATIC]
                  [--mp_hands_draw MP_HANDS_DRAW]
                  [--mp_hands_csv MP_HANDS_CSV] [--mp_faces_max MP_FACES_MAX]
                  [--mp_faces_static MP_FACES_STATIC]
                  [--mp_faces_draw MP_FACES_DRAW]
                  [--mp_faces_csv MP_FACES_CSV]
                  [--mp_poses_static MP_POSES_STATIC]
                  [--mp_poses_draw MP_POSES_DRAW]
                  [--mp_poses_csv MP_POSES_CSV]

Uses MediaPipe to generate CSV files from videos or camera live streams with
the records of the tracked movements.

optional arguments:
  -h, --help            show this help message and exit
  --video VIDEO         [filename] Existing video to be tracked. If empty,
                        --camera will be used
  --camera CAMERA       [int] Number that identifies the camera to use
  --interval INTERVAL   [int] Number of records to store in memory before
                        writing them
  --first FIRST         [int] First frame to capture
  --last LAST           [int] Last frame to capture
  --flip FLIP           [yes|no] Flip the image horizontally
  --mp_hands_max MP_HANDS_MAX
                        [int] Maximum number of hands to track with MediaPipe
  --mp_hands_static MP_HANDS_STATIC
                        [yes|no] Tell MediaPipe to run a continuous detection
                        of hands
  --mp_hands_draw MP_HANDS_DRAW
                        [yes|no] Draw MediaPipe's records of tracked hands
  --mp_hands_csv MP_HANDS_CSV
                        [filename] Export a CSV file with MediaPipe's records
                        of tracked hands
  --mp_faces_max MP_FACES_MAX
                        [int] Maximum number of faces to track with MediaPipe
  --mp_faces_static MP_FACES_STATIC
                        [yes|no] Tell MediaPipe to run a continuous detection
                        of faces
  --mp_faces_draw MP_FACES_DRAW
                        [yes|no] Draw MediaPipe's records of tracked faces
  --mp_faces_csv MP_FACES_CSV
                        [filename] Export a CSV file with MediaPipe's records
                        of tracked faces
  --mp_poses_static MP_POSES_STATIC
                        [yes|no] Tell MediaPipe to run a continuous detection
                        of poses
  --mp_poses_draw MP_POSES_DRAW
                        [yes|no] Draw MediaPipe's records of tracked poses
  --mp_poses_csv MP_POSES_CSV
                        [filename] Export a CSV file with MediaPipe's records
                        of tracked poses
```
