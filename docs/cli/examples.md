# Examples of Command Line Usage

## Track from a camera

Track hand movements with Mediapipe from the first camera and store the results in a compressed CSV file named **camera.csv.gz**.

```bash
trucotrack \
  --camera 0 \
  --mp_hands_csv camera.csv.gz
```

This command won't stop tracking until you press Esc.

## Track the first frame from a camera

Track one frame from the first camera and store the results in a file named **still.csv.gz**.

```bash
trucotrack \
  --camera 0 \
  --last 1 \
  --mp_hands_csv still.csv.gz
```

## Track a range of frames from a video

Track the video **recording.mp4** from frame **50** to **55** and save the results in a
file named **from-50-to-55.csv.gz**.

```bash
trucotrack \
  --video recording.mp4 \
  --first 50 --last 55 \
  --mp_hands_csv from-50-to-55.csv.gz
```
