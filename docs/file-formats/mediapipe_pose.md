# File Format for MediaPipe Pose Tracking

![Screenshot of a pose capture](https://gitlab.com/hawara.es/blender/trucotrack/-/raw/main/docs/file-formats/examples/poses.png)

There is an example of this file format available at [docs/file-formats/examples/poses.csv.gz](https://gitlab.com/hawara.es/blender/trucotrack/-/blob/main/docs/file-formats/examples/poses.csv.gz) as long as its uncompressed version [docs/file-formats/examples/poses.csv](https://gitlab.com/hawara.es/blender/trucotrack/-/blob/main/docs/file-formats/examples/poses.csv).

## Detail of Columns

The first columns in the file describe what's being tracked:

| Column Name | Type | Description |
| --- | --- | --- |
| frame_number | Integer | Number of the frame that the line represents |
| frame_width | Integer | Number of pixels in the horizontal axis |
| frame_height | Integer | Number of pixels in the vertical axis |

Note that there is no **pose_index** as MediaPipe only tracks one pose at a time.

The rest of the columns correspond to node positions in three orthogonal axis, **x**, **y** and **z**. In order to simplify the following list, we'll write **nose_{x|y|z}** for the trio of columns
**nose_x**, **nose_y** and **nose_z**.

| Column Name | Type | Description |
| --- | --- | --- |
| nose_{x\|y\|z} | Float | Position of the marker in each of the axes |
| left_eye_inner_{x\|y\|z} | Float | Position of the marker in each of the axes |
| left_eye_{x\|y\|z} | Float | Position of the marker in each of the axes |
| left_eye_outer_{x\|y\|z} | Float | Position of the marker in each of the axes |
| right_eye_inner_{x\|y\|z} | Float | Position of the marker in each of the axes |
| right_eye_{x\|y\|z} | Float | Position of the marker in each of the axes |
| right_eye_outer_{x\|y\|z} | Float | Position of the marker in each of the axes |
| left_ear_{x\|y\|z} | Float | Position of the marker in each of the axes |
| right_ear_{x\|y\|z} | Float | Position of the marker in each of the axes |
| mouth_left_{x\|y\|z} | Float | Position of the marker in each of the axes |
| mouth_right_{x\|y\|z} | Float | Position of the marker in each of the axes |
| left_shoulder_{x\|y\|z} | Float | Position of the marker in each of the axes |
| right_shoulder_{x\|y\|z} | Float | Position of the marker in each of the axes |
| left_elbow_{x\|y\|z} | Float | Position of the marker in each of the axes |
| right_elbow_{x\|y\|z} | Float | Position of the marker in each of the axes |
| left_wrist_{x\|y\|z} | Float | Position of the marker in each of the axes |
| right_wrist_{x\|y\|z} | Float | Position of the marker in each of the axes |
| left_pinky_{x\|y\|z} | Float | Position of the marker in each of the axes |
| right_pinky_{x\|y\|z} | Float | Position of the marker in each of the axes |
| left_index_{x\|y\|z} | Float | Position of the marker in each of the axes |
| right_index_{x\|y\|z} | Float | Position of the marker in each of the axes |
| left_thumb_{x\|y\|z} | Float | Position of the marker in each of the axes |
| right_thumb_{x\|y\|z} | Float | Position of the marker in each of the axes |
| left_hip_{x\|y\|z} | Float | Position of the marker in each of the axes |
| right_hip_{x\|y\|z} | Float | Position of the marker in each of the axes |
| left_knee_{x\|y\|z} | Float | Position of the marker in each of the axes |
| right_knee_{x\|y\|z} | Float | Position of the marker in each of the axes |
| left_ankle_{x\|y\|z} | Float | Position of the marker in each of the axes |
| right_ankle_{x\|y\|z} | Float | Position of the marker in each of the axes |
| left_heel_{x\|y\|z} | Float | Position of the marker in each of the axes |
| right_heel_{x\|y\|z} | Float | Position of the marker in each of the axes |
| left_foot_index_{x\|y\|z} | Float | Position of the marker in each of the axes |
| right_foot_index_{x\|y\|z} | Float | Position of the marker in each of the axes |
