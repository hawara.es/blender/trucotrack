# File Format for MediaPipe Hand Tracking

![Screenshot of a hand capture](https://gitlab.com/hawara.es/blender/trucotrack/-/raw/main/docs/file-formats/examples/hands.png)

There is an example of this file format available at [docs/file-formats/examples/hands.csv.gz](https://gitlab.com/hawara.es/blender/trucotrack/-/blob/main/docs/file-formats/examples/hands.csv.gz) as long as its uncompressed version [docs/file-formats/examples/hands.csv](https://gitlab.com/hawara.es/blender/trucotrack/-/blob/main/docs/file-formats/examples/hands.csv).

## Detail of Columns

The first columns in the file describe what's being tracked:

| Column Name | Type | Description |
| --- | --- | --- |
| frame_number | Integer | Number of the frame that the line represents |
| frame_width | Integer | Number of pixels in the horizontal axis |
| frame_height | Integer | Number of pixels in the vertical axis |
| hand_index | Integer | Number of the hand in the actual scene |
| hand_score | Float | Confidence assigned to the detection of the hand |
| hand_handedness | String | Handedness (left or right) of the hand |

The rest of the columns correspond to node positions in three orthogonal axis, **x**, **y** and **z**. In order to simplify the following list, we'll write **wrist_{x|y|z}** for the trio of columns
**wrist_x**, **wrist_y** and **wrist_z**.

| Column Name | Type | Description |
| --- | --- | --- |
| wrist_{x\|y\|z} | Float | Position of the marker in each of the axes |
| thumb_cmc_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| thumb_mcp_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| thumb_ip_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| thumb_tip_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| index_finger_mcp_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| index_finger_pip_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| index_finger_dip_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| index_finger_tip_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| middle_finger_mcp_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| middle_finger_pip_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| middle_finger_dip_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| middle_finger_tip_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| ring_finger_mcp_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| ring_finger_pip_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| ring_finger_dip_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| ring_finger_tip_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| pinky_mcp_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| pinky_pip_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| pinky_dip_{x\|y\|z\} | Float | Position of the marker in each of the axes |
| pinky_tip_{x\|y\|z\} | Float | Position of the marker in each of the axes |
