# File Format for MediaPipe Face Tracking

![Screenshot of a face capture](https://gitlab.com/hawara.es/blender/trucotrack/-/raw/main/docs/file-formats/examples/faces.png)

There is an example of this file format available at [docs/file-formats/examples/faces.csv.gz](https://gitlab.com/hawara.es/blender/trucotrack/-/blob/main/docs/file-formats/examples/faces.csv.gz) as long as its uncompressed version [docs/file-formats/examples/faces.csv](https://gitlab.com/hawara.es/blender/trucotrack/-/blob/main/docs/file-formats/examples/faces.csv).

## Detail of Columns

The first columns in the file describe what's being tracked:

| Column Name | Type | Description |
| --- | --- | --- |
| frame_number | Integer | Number of the frame that the line represents |
| frame_width | Integer | Number of pixels in the horizontal axis |
| frame_height | Integer | Number of pixels in the vertical axis |
| face_index | Integer | Number of the face in the actual scene (this index is built by TrucoTrack) |

The rest of the columns correspond to node positions in three orthogonal axis, **x**, **y** and **z**. You'll find 468 nodes in the file. For each of them, the positions in the three axis are provided.

| Column Name | Type | Description |
| --- | --- | --- |
| face_{n}_{x\|y\|z} | Float | Position of the marker in each of the axes |
