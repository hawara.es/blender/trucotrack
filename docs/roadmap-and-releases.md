# Roadmap and Releases

## Roadmap (and Work In Progress)

### Hopefully in 2021 / v1.0.0

These features will be added:

- option to enable/disable compression
- option to print rather than creating a file
- a description of its Python API

## History of Releases

### 13th October, 2021 / v0.0.5

This release adds:

- `frame_width` and `frame_height` to csv files

### 16th September, 2021 / v0.0.4.2

This release fixes:

- flip wasn't working properly

### 16th September, 2021 / v0.0.4.1

This release fixes:

- rename `hand_handness` as `hand_handedness`

### 9th September, 2021 / v0.0.4

This release adds support for:

- CSV columns now use the names of bones when capturing poses

### 5th September, 2021 / v0.0.3.1

This release fixes:

- missing line breaks in CSV files

### 5th September, 2021 / v0.0.3

This release adds support for:

- track human poses using MediaPipe
- video-based examples (rather than camera-based)

### 27th August, 2021 / v0.0.2

This release adds support for:

- a documentation folder with:
    - detailed description and example of the generated file format for hand tracks
    - complete list of functions added with each release
    - roadmap describing planned work and ideas for future improvements
    - a guide to contribute with the development
- an independent class for frame dispatching
- compression for all generated CSV files

### 23rd August, 2021 / v0.0.1

This release adds support for:

- command line usage for tracking movement from videos and camera streams:
    - having control over how frequent the gathered data will be saved
    - *optionally* limiting the first and last frames
    - *optionally* flipping the frame (useful when tracking from cameras)
- track hand movements using MediaPipe:
    - *optionally* limiting the number of hands to track
    - *optionally* drawing the tracking points
    - *optionally* enabling dynamic detection (otherwise, only the first frames are used)
