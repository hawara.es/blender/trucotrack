# How to Contribute with the Development

To contribute with the project development you may want to start by cloning it from its Git repository:

```bash
git clone https://gitlab.com/hawara.es/blender/trucotrack \
  && cd trucotrack
```

and then install the dependencies:

```bash
pip install -r requirements.txt
```

## Clear the Python cache

If you make some changes and the code seems to be running an old version that doesn't reflect what you changed, try running:

```bash
source tools/clean/clean.sh
```

## Prepare a release

Before every release, all examples must be updated. To automatically (re)generate them, go to the project folder and run:

```bash
source tools/build/build.sh
```

## Dependencies

TrucoTrack depends on:

- mediapipe
- opencv

If some changes involve adding a new library, an update will be probably necessary for both the `requirements.txt` and `setup.py` files.

To automatically update `requirements.txt`, install **pipreqs** and run:

```bash
# this will update requirements.txt
pipreqs --force
```

To update `setup.py`, open it and manually check the **install_requires** key:

```python
install_requires=['mediapipe', 'opencv-python'],
```
