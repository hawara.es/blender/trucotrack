# TrucoTrack - Command line client for motion capture tools

TrucoTrack is a program intended to serve as a helper when using motion capture software (like MediaPipe). It defines a CSV file format for each of the available trackers and lets you easily call them from a command line.

## Usage

- [Examples of Command Line Usage](https://gitlab.com/hawara.es/blender/trucotrack/-/blob/main/docs/cli/examples.md)
- [Display the Command Line Help](https://gitlab.com/hawara.es/blender/trucotrack/-/blob/main/docs/cli/help.md)

## Documentation

### File Formats

- [File Format for MediaPipe Hand Tracks](https://gitlab.com/hawara.es/blender/trucotrack/-/blob/main/docs/file-formats/mediapipe_hand.md)
- [File Format for MediaPipe Face Tracks](https://gitlab.com/hawara.es/blender/trucotrack/-/blob/main/docs/file-formats/mediapipe_face.md)
- [File Format for MediaPipe Pose Tracks](https://gitlab.com/hawara.es/blender/trucotrack/-/blob/main/docs/file-formats/mediapipe_pose.md)

### Development

Check the [releases](https://gitlab.com/hawara.es/blender/trucotrack/-/releases) and our [roadmap and releases](https://gitlab.com/hawara.es/blender/trucotrack/-/blob/main/docs/roadmap-and-releases.md) document. Only versions 0.x.x will be released until the tool is ready for production. (If ever.)

- [How to Contribute with the Development](https://gitlab.com/hawara.es/blender/trucotrack/-/blob/main/docs/how-to-contribute-with-the-development.md)
- TrucoTrack has been released under a [GPLv3 License](https://gitlab.com/hawara.es/blender/trucotrack/-/blob/main/LICENSE.txt)

## Warning: Unfinished Work

Please take into account that this is an experimental software still in development. Unfinished, unpolished, incomplete and probably buggy. If you use it, please expect some inconsistencies and crashes.
