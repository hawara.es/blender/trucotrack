# This scripts helps to keep the command line help
# example in the documentation.
#
# To execute it, run source tools/update-cli-help.sh
# from the root of the project.

help_file=docs/cli/help.md

read -r -d '' content <<'EOF'
Use `-h` to display the command line help.

```bash
# if you installed `trucotrack` as a global app
trucotrack -h

# if you are working with the source code:
python -m src -h
```
EOF

echo "# Command Line / Help" > $help_file
echo "" >> $help_file
echo "$content" >> $help_file
echo "\`\`\`" >> $help_file
echo "" >> $help_file
trucotrack -h >> $help_file
echo "\`\`\`" >> $help_file
