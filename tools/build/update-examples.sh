EXAMPLES_FOLDER=docs/file-formats/examples/

rm -rf $EXAMPLES_FOLDER/*.csv
rm -rf $EXAMPLES_FOLDER/*.csv.gz

python -m src \
    --video $EXAMPLES_FOLDER/hands.mp4 \
    --mp_hands_csv $EXAMPLES_FOLDER/hands.csv.gz \
    --last 1

python -m src \
    --video $EXAMPLES_FOLDER/faces.mp4 \
    --mp_faces_csv $EXAMPLES_FOLDER/faces.csv.gz \
    --last 1

python -m src \
    --video $EXAMPLES_FOLDER/poses.mp4 \
    --mp_poses_csv $EXAMPLES_FOLDER/poses.csv.gz \
    --last 1

gzip -d -k $EXAMPLES_FOLDER/hands.csv.gz
gzip -d -k $EXAMPLES_FOLDER/faces.csv.gz
gzip -d -k $EXAMPLES_FOLDER/poses.csv.gz
