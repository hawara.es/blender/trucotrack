#!/usr/bin/env bash

echo "Ensuring that development requirements are installed"
python3 -m pip install --upgrade pip build twine &> /dev/null

echo "Clearing previous builds"
rm -rf dist/* &> /dev/null

echo "Uninstalling versions installed with pip"
pip3 uninstall trucotrack -y &> /dev/null

echo "Building the project"
python3 -m build &> /dev/null

echo "Running the install script"
sudo python3 setup.py install &> /dev/null

echo "Updating the help snapshot"
source tools/build/update-cli-help.sh &> /dev/null

echo "Updating the example files"
source tools/build/update-examples.sh &> /dev/null

echo ""
echo "To upload the new files to pypi.org, run:"
echo "> twine upload dist/*"
